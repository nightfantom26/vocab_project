from abc import ABC, abstractmethod

from schema.document_schema import DocumentSchema
from schema.user_schema import User


class IVocabularSource(ABC):

    @abstractmethod
    def get_description(self, user: User, word: str) -> DocumentSchema:
        pass
