import logging

from googletrans import Translator

from constants.app_constant import BEHAVIOUR_LOGGER
from integration.google.google_response_parser import GoogleResponseParser
from integration.ivocabular_source import IVocabularSource
from schema.document_schema import DocumentSchema
from schema.user_schema import User


class GoogleTranslate(IVocabularSource):

    def __init__(self):
        self.__translator = Translator()
        self.__parser = GoogleResponseParser()

    def get_description(self, user: User, word: str) -> DocumentSchema:
        logging.getLogger(BEHAVIOUR_LOGGER).info("Sending request in GoogleTranslate", user.model_dump())
        word_description = self.__translator.translate(word, src="en", dest="ru")
        logging.getLogger(BEHAVIOUR_LOGGER).info("Got response from GoogleTranslate ", user.model_dump())
        result = self.__parser.parse(word_description.extra_data)
        result["word"] = word
        return DocumentSchema(**result)
