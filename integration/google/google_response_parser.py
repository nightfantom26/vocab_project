class GoogleResponseParser:

    def parse_definition(self, data):
        description = data.get("definitions")
        if description is None:
            description = []
        def_dict = {}
        for record in description:
            def_list = []
            for definition_variant in record[1]:
                container = {"definition": definition_variant[0]}
                if len(definition_variant) > 2 and definition_variant[2] is not None:
                    container["example"] = definition_variant[2]
                def_list.append(container)
            def_dict[record[0]] = def_list
        return def_dict

    def parse_synonyms(self, data):
        description = data.get("synonyms")
        if description is None:
            description = []
        POS_synonyms = {}
        for record in description:
            words = []
            words_list_meta = record[1]
            for syn_record in words_list_meta:
                words.extend(syn_record[0])
            POS_synonyms[record[0]] = words
        return POS_synonyms

    def parse(self, data):
        translation = data["translation"][0][0]
        synonyms = self.parse_synonyms(data)
        definition = self.parse_definition(data)
        return {"translation": translation, "synonyms": synonyms, "definition": definition}
