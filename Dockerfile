FROM python:3.10.12-slim

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
COPY . /app
WORKDIR /app
CMD ["python", "-m", "uvicorn", "app:app",  "--log-config", "/app/configs/logging.yml", "--host", "0.0.0.0"]