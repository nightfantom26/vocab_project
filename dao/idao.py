from abc import ABC, abstractmethod
from typing import List

from schema.document_schema import DocumentSchema
from schema.search_schema import SearchSchema


class IDAO(ABC):

    @abstractmethod
    def get_by_word(self, word: str) -> DocumentSchema:
        pass

    @abstractmethod
    def save_word_description(self, word: str, data: DocumentSchema):
        pass

    @abstractmethod
    def delete_word(self, word: str) -> None:
        pass

    @abstractmethod
    def search(self, search_request: SearchSchema) -> List[DocumentSchema]:
        pass

    @abstractmethod
    def close(self) -> None:
        pass