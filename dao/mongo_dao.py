import logging
from typing import List

from pydantic import TypeAdapter
from pymongo import MongoClient

from dao.idao import IDAO
from schema.document_schema import DocumentSchema
from schema.search_schema import SearchSchema
from settings.mongodb_settings_model import MongoDBSettings


class MongoDBDAO(IDAO):
    HOST = "host"
    PORT = "port"

    def __init__(self, config: MongoDBSettings):
        host = config.uri.hosts()[0]
        params = {
            MongoDBDAO.HOST: host[MongoDBDAO.HOST],
            MongoDBDAO.PORT: host[MongoDBDAO.PORT]
        }
        logging.getLogger(__name__).info(f"Connecting to MongoDB %({MongoDBDAO.HOST})s:%({MongoDBDAO.PORT})s", params)
        self.__client = MongoClient(config.uri.unicode_string())
        self.__db = self.__client[config.database]
        self.__config = config

    def get_by_word(self, word: str) -> DocumentSchema:
        collection = self.__db[self.__config.collection]
        data = collection.find_one({"word": word})
        result = None
        if data is not None:
            result = DocumentSchema(**data)
        return result

    def save_word_description(self, word: str, data: DocumentSchema):
        collection = self.__db[self.__config.collection]
        insert_result = collection.insert_one(data.model_dump())

    def delete_word(self, word: str) -> None:
        collection = self.__db[self.__config.collection]
        collection.delete_one({"word": word})

    def search(self, search_request: SearchSchema) -> List[DocumentSchema]:
        collection = self.__db[self.__config.collection]
        sort = [("word", search_request.sorting.value)] if search_request.sorting is not None else None
        query = search_request.query if search_request.query is not None else ""
        projection = ["word"]
        dump = search_request.model_dump()
        for field in ["definition", "synonyms", "translation"]:
            if dump.get(field, False):
                projection.append(field)

        cursor = collection.find({"word": {"$regex": query}}, sort=sort, projection=projection,
                                 skip=search_request.skip, limit=search_request.limit)
        return TypeAdapter(List[DocumentSchema]).validate_python(list(cursor))

    def close(self) -> None:
        self.__client.close()
