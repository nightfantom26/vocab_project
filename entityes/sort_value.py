from enum import IntEnum


class SortValue(IntEnum):
    ASCENDING = 1
    DESCENDING = -1
