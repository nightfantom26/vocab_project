from fastapi import Path


async def normilize_word(word: str = Path(max_length=20)) -> str:
    return word.lower()
