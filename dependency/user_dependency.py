import time
from secrets import token_urlsafe

from fastapi import Request, Response

from constants.app_constant import SESSION_COOKIE_NAME
from schema.user_schema import User


async def create_user(request: Request, response: Response) -> User:
    session = request.cookies.get(SESSION_COOKIE_NAME)
    if not session:
        session = token_urlsafe(16)
        response.set_cookie(key=SESSION_COOKIE_NAME, value=session, max_age=7 * 24 * 60 * 60)
    return User(session_id=session, message_id=int(time.time()))
