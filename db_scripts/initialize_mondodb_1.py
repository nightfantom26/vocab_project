import logging
import logging.config
import os

from pymongo import MongoClient


from settings.app_settings import AppSettings
from settings.conf_reader import get_log_config, read_app_config

HOST = "host"
PORT = "port"


def run():
    log_conf = get_log_config("../configs/logging.yml")
    logging.config.dictConfig(log_conf)
    config_dict = read_app_config(def_path="../configs/app_conf.yml")
    config = AppSettings(**config_dict)
    mongodb_conf = config.mongodb
    host = mongodb_conf.uri.hosts()[0]
    params = {
        HOST: host[HOST],
        PORT: host[PORT]
    }
    logger = logging.getLogger(__name__)
    logger.info(f"Connecting to MongoDB %({HOST})s:%({PORT})s", params)
    client = MongoClient(mongodb_conf.uri.unicode_string())
    db = client[mongodb_conf.database]
    logger.info(f"Creating collection '{mongodb_conf.collection}'")
    db.create_collection(mongodb_conf.collection)
    logger.info("Creating index")
    db[mongodb_conf.collection].create_index("word")
    logger.info("Done")


if __name__ == "__main__":
    run()
