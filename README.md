# Load analysis

Based on business requirements it was selected to support availability and partition tolerance

- In order to store one record we need approximately 16 KB
- Average amount of words in a language 400.000
- For one language we need ~ 6 GB disc space
- For keeping all languages supported by Google (128 languages) ~ 781 GB disc space

MongoDB is suitable database for this requirements. It supports replication and sharding.

# Scalability and fault tolerance
- The service does not keep user state that's why we can move user from one instance to another in any moment
- The service is lightweight. We can quickly increase amount of instances depends on load
- MongoDB is horizontally scalable, supports replication and sharding. We can increase amount of instances depend on 
load and store huge data between shards  

# Features

- Logging user actions with session id and message id
- Online monitoring with prometheus
- Flexible parameters configuration powered by PyDantic Settings

# API

API is available by path HOST/docs

# Run

## Before start (optional)

For better DB request performance execute initializing DB script in db_scripts folder. It creates DB, collection and index

```shell
python initialize_mondodb_1.py
```

## Docker

File docker-compose.yml contains minimal working environment consist of Vocab service and MongoDB

For start type in console

```shell
docker-compose up
```

# Next steps

Next actions should be made in future releases:

- Support changing source and target languages
- Support fully async in communication with data service provider and database
- Add unit and system tests
- Implement CI/CD
- Migrate from public unstable Google web API to stable paid Google API
- Support TTL for record in DB and update after TTL is expired