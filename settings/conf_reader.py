import logging
import os
from typing import Any, Dict

import yaml


def read_app_config(def_path: str = "./configs/app_conf.yml") -> Dict[str, Any]:
    path = os.environ.get("APP_CONF_PATH", def_path)
    logging.getLogger(__name__).info(f"Reading AppConf {path}")
    with open(path) as file:
        config_dict = yaml.safe_load(file)
    return config_dict


def get_log_config(def_path: str = "./configs/logging.yml") -> Dict[str, Any]:
    path = os.environ.get("LOG_PATH", def_path)
    with open(path) as file:
        config_dict = yaml.safe_load(file)
    return config_dict
