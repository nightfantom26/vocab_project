from pydantic import BaseModel, MongoDsn, Field
from pydantic_core import MultiHostUrl


class MongoDBSettings(BaseModel):
    uri: MongoDsn = Field(MultiHostUrl("mongodb://127.0.0.1:27017"))
    collection: str = Field("vocab_collection")
    database: str = Field("vocab_db")
