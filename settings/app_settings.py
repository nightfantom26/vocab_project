from pydantic import Extra
from pydantic_settings import BaseSettings, SettingsConfigDict

from settings.mongodb_settings_model import MongoDBSettings


class AppSettings(BaseSettings):
    mongodb: MongoDBSettings
    model_config = SettingsConfigDict(case_sensitive=False, env_nested_delimiter='__', extra=Extra.allow)

