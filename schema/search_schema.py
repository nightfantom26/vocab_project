from pydantic import BaseModel, Field

from entityes.sort_value import SortValue


class SearchSchema(BaseModel):
    query: str | None = None
    sorting: SortValue | None = None
    definition: bool | None = False
    synonyms: bool | None = False
    translation: bool | None = False
    limit: int | None = Field(10, ge=1, le=100)
    skip: int | None = Field(0, ge=0)

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "query": "run",
                    "sorting": 1,
                    "definition": True,
                    "synonyms": True,
                    "translation": True,
                    "skip": 0,
                    "limit": 10
                }
            ]
        }
    }
