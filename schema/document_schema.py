from typing import Dict, List

from pydantic import BaseModel


class Definition(BaseModel):
    definition: str | None = None
    example: str | None = None


class DocumentSchema(BaseModel):
    word: str
    translation: str | None = None
    synonyms: Dict[str, List[str]] | None = None
    definition: Dict[str, List[Definition]] | None = None

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "word": "run",
                    "translation": "бегать",
                    "synonyms": {
                        "глагол": [
                            "sprint",
                            "race",
                            "dart"
                        ],
                        "имя существительное": [
                            "sprint",
                            "race",
                            "dash"
                        ]
                    },
                    "definition": {
                        "глагол": [
                            {
                                "definition": "move at a speed faster than a walk, never having both or all the feet on the ground at the same time.",
                                "example": "the dog ran across the road"
                            },
                            {
                                "definition": "pass or cause to pass quickly or smoothly in a particular direction.",
                                "example": "the rumor ran through the pack of photographers"
                            }
                        ],
                        "имя существительное": [
                            {
                                "definition": "an act or spell of running.",
                                "example": "I usually go for a run in the morning"
                            },
                            {
                                "definition": "a journey accomplished or route taken by a vehicle, aircraft, or boat, especially on a regular basis.",
                                "example": "the New York-Washington run"
                            }
                        ]
                    }
                }
            ]
        }
    }
