from pydantic import BaseModel


class User(BaseModel):
    session_id: str
    message_id: int
