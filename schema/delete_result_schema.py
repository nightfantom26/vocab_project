from pydantic import BaseModel


class DeleteResultSchema(BaseModel):
    description: str
