import logging
from contextlib import asynccontextmanager
from typing import Annotated, List

from fastapi import FastAPI, Depends, status, Response, Body
from prometheus_fastapi_instrumentator import Instrumentator

from constants.app_constant import BEHAVIOUR_LOGGER
from dao.idao import IDAO
from dao.mongo_dao import MongoDBDAO
from dependency.user_dependency import create_user
from dependency.word_dependency import normilize_word
from integration.google.google_translate import GoogleTranslate
from integration.ivocabular_source import IVocabularSource
from schema.delete_result_schema import DeleteResultSchema
from schema.document_schema import DocumentSchema
from schema.search_schema import SearchSchema
from schema.user_schema import User
from settings.app_settings import AppSettings
from settings.conf_reader import read_app_config

WordDescription = Annotated[str, Depends(normilize_word)]
UserDescription = Annotated[User, Depends(create_user)]
WORD_KEY = "word"


@asynccontextmanager
async def lifespan(app: FastAPI):
    logging.getLogger(__name__).info("Initializing app")
    config_dict = read_app_config()
    settings = AppSettings(**config_dict)
    app.vocabular_source = GoogleTranslate()
    app.instrumentator.expose(app)
    logging.getLogger(__name__).info("Initializing DB")
    app.dao = MongoDBDAO(settings.mongodb)
    logging.getLogger(__name__).info("Initializing DB - Done")
    logging.getLogger(__name__).info("Initializing app - Done")
    yield
    logging.getLogger(__name__).info("Closing DB")
    app.dao.close()
    logging.getLogger(__name__).info("Closing DB - Done")
    logging.shutdown()


app = FastAPI(lifespan=lifespan)
app.instrumentator = Instrumentator().instrument(app)


@app.get("/v1/word/{word}")
async def get_word_detail(user: UserDescription, word: WordDescription) -> DocumentSchema:
    """
    The method return full word description. If the word exists in DB the service returns it
    otherwise request will be sent in Google Translation API
    """
    log_params = user.model_dump()
    log_params[WORD_KEY] = word
    logger = logging.getLogger(BEHAVIOUR_LOGGER)
    logger.info(f"Requested word %({WORD_KEY})s", log_params)
    dao: IDAO = app.dao
    logger.info(f"Checking word %({WORD_KEY})s in DB", log_params)
    description = dao.get_by_word(word)
    if not description:
        logger.info(f"Word %({WORD_KEY})s is missing", log_params)
        source: IVocabularSource = app.vocabular_source
        description = source.get_description(user, word)
        logger.info(f"Saving word %({WORD_KEY})s in DB", log_params)
        dao.save_word_description(word, description)
        logger.info(f"Saving word %({WORD_KEY})s in DB - Done", log_params)
    else:
        logger.info(f"There is word %({WORD_KEY})s in DB", log_params)
    return description


@app.post("/v1/word")
async def get_words(user: UserDescription, search_request: Annotated[SearchSchema, Body()]) -> \
        List[DocumentSchema]:
    """
    Ger list of words from database
    """
    log_params = user.model_dump()
    logger = logging.getLogger(BEHAVIOUR_LOGGER)
    logger.info(f"Requesting list of words", log_params)
    dao: IDAO = app.dao
    result = dao.search(search_request)
    return result


@app.delete("/v1/word/{word}",
            responses={
                200: {"description": "Successful Response", "model": DeleteResultSchema},
                400: {"description": "Word is not found", "model": DeleteResultSchema}}
            )
async def delete_word(user: UserDescription, word: WordDescription, response: Response) -> DeleteResultSchema:
    """
    Delete word 'WORD' from dictionary
    """
    log_params = user.model_dump()
    log_params[WORD_KEY] = word
    logger = logging.getLogger(BEHAVIOUR_LOGGER)
    logger.info(f"Deleting word %({WORD_KEY})s", log_params)
    dao: IDAO = app.dao
    description = dao.get_by_word(word)
    if description:
        dao.delete_word(word)
        response.status_code = status.HTTP_200_OK
        message = "Success"
        logger.info(f"Deleting word %({WORD_KEY})s - Done", log_params)
    else:
        response.status_code = status.HTTP_400_BAD_REQUEST
        message = f"There is not word {word}"
        logger.info(f"Deleting word %({WORD_KEY})s - Fail", log_params)
    return DeleteResultSchema(description=message)
